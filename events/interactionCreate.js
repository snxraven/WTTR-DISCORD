const client = require("../wttr_bot");
require("dotenv").config();
const { glob } = require("glob");
const { promisify } = require("util");
const globPromise = promisify(glob);

client.on("interactionCreate", async (interaction) => {

  // Slash Commands
  const slashCommands = await globPromise(`${process.cwd()}/commands/*/*.js`);
  const arrayOfSlashCommands = [];
 
  // Map the slash commands into data to be processed
  slashCommands.map((value) => {
      const file = require(value);
      const splitted = value.split("/");
      const directory = splitted[splitted.length - 2];

      if (!file?.name) return;

      const properties = {
          directory,
          ...file
      };
      client.slashCommands.set(file.name, properties);

      if (["MESSAGE", "USER"].includes(file.type)) delete file.description;

      // Push the data
      arrayOfSlashCommands.push(file);
  });


  // Slash Command Handling
  if (interaction.isChatInputCommand()) {

      // Grabbing Command Data for this interaction
      let commandData = []

      // We use ForEach here to filter our array into the single commands info.
      await arrayOfSlashCommands.forEach(command => {
          if (command.name == interaction.commandName) {
              commandData.push(command)
          }
      });

      // Process and Parse Data 
      let dataToProcess = JSON.stringify(commandData[0])
      let parsedData = JSON.parse(dataToProcess)


    if (interaction.commandName == "modal-example"){
        console.log("Modal - Skipping defer")
      } else {
      // If the command is private, set ephemeral true else, set false
      console.log(parsedData)
      if (parsedData.private == true) {
          await interaction.deferReply({
              ephemeral: true
          }).catch(() => {});

      } else {
          await interaction.deferReply({
              ephemeral: false
          }).catch(() => {});
      }
    }
      const cmd = client.slashCommands.get(interaction.commandName);
      if (!cmd)
          return interaction.followUp({
              content: "An error has occurred "
          });

      const args = [];

      for (let option of interaction.options.data) {
          if (option.type === "SUB_COMMAND") {
              if (option.name) args.push(option.name);
              option.options?.forEach((x) => {
                  if (x.value) args.push(x.value);
              });
          } else if (option.value) args.push(option.value);
      }
      interaction.member = interaction.guild.members.cache.get(interaction.user.id);

      cmd.run(client, interaction, args);
  }

  // Context Menu Handling
  if (interaction.isContextMenuCommand()) {
      await interaction.deferReply({
          ephemeral: false
      });
      const command = client.slashCommands.get(interaction.commandName);
      if (command) command.run(client, interaction);
  }
});