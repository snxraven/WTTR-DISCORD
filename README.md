# WTTR BOT

Only 3 dependencies required to run this bot!

Message intents are NOT supported in this bot, this is due to the verification that Discord is enabling.

Structure: 

**commands** - This folder contains commands

**event** - This folder contains files related to discord.js events. (Like "ready", "interactionCreate")

**handler**  - This folder contains files that read the commands folders contents.

**wttr_bot.js** - This is the main file to run the bot.



1) Use ```npm i ```

2) Create a .env file ``` touch .env```

3) Edit .env 
```
TOKEN = token
```

4) Run the bot ```node wttr_bot.js```


Want to make this better? Issue a pull request!
