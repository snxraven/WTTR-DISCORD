const { EmbedBuilder } = require('discord.js');
const fetch = require('node-fetch'); // Adding the fetch library for HTTP requests

module.exports = {
  name: "weather",
  description: "Get the weather for a location",
  private: false,
  options: [{
    name: "location",
    description: "The location you would like to check",
    required: true,
    type: 3 // 3 is type STRING
  }],
  run: async (client, interaction) => {
    const location = interaction.options.getString('location');

    // Check if the image exists
    const imageUrl = `https://wttr.in/${encodeURIComponent(location)}.png?u`;
    try {
      const response = await fetch(imageUrl);
      if (!response.ok) {
        throw new Error("Image not found");
      }
    } catch (error) {
      return interaction.editReply("Error: The weather image could not be found.");
    }

    const embed = new EmbedBuilder()
      .setColor("#FF0000")
      .setTitle(`The weather for: ${location}`)
      .setTimestamp()
      .setImage(imageUrl)
      .setFooter({ text: `Provided by wttr.in`, iconURL: `${interaction.user.displayAvatarURL()}` });

    await interaction.editReply({ embeds: [embed] });
  },
};